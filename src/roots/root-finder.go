package roots

import "math"

type Bounds struct {
	upper float64
	lower float64
	midPointX float64
	y float64
}

func FunctionEvaluator (x float64) float64 {
	var result = math.Pow(x, 2) + 6 * x + 2
	return result
}

func CalculateMidpointX(lowerBounds float64, upperBounds  float64) float64 {
	return (lowerBounds + upperBounds) / 2
}

func mutateBounds ( bounds Bounds) Bounds {
	bounds.midPointX = CalculateMidpointX(bounds.lower, bounds.upper)
	y := FunctionEvaluator(bounds.midPointX)
	if y >= 0 { // positive result
		bounds.upper = bounds.midPointX
	}else{ // negative result
		bounds.lower = bounds.midPointX
	}
	bounds.y = y
	return bounds
}

func FindRootByIteration(lowerBounds float64, upperBounds  float64) float64 {
	iterations := 10
	bounds := Bounds{midPointX : math.NaN(), lower: lowerBounds, upper: upperBounds}
	for i := 0; i < iterations; i++ {
		bounds = mutateBounds (bounds)
	}
	return bounds.midPointX
}

func FindRootByPrecision(lowerBounds float64, upperBounds  float64) float64 {
	precision := .1
	var y = float64(10000)
	bounds := Bounds{midPointX : math.NaN(), lower: lowerBounds, upper: upperBounds, y: y}
	for y > precision || y < -precision {
		bounds = mutateBounds (bounds)
		y = bounds.y
	}
	return bounds.midPointX
}