package roots

import (
	"github.com/magiconair/properties/assert"
	"math"
	"testing"
)

var lowerBound = float64(-2)
var upperBound = float64(2)

func Test_functionEvaluatorForA(t *testing.T) {
	// WHEN
	fx := FunctionEvaluator(lowerBound)

	// THEN
	assert.Equal(t, fx, float64(-6))
}

func Test_functionEvaluatorForB(t *testing.T) {
	// WHEN
	fx := FunctionEvaluator(upperBound)

	// THEN
	assert.Equal(t, fx, float64(18))
}

func Test_calculateMiddleX(t *testing.T) {
	// WHEN
	fx := CalculateMidpointX(lowerBound, upperBound)

	// THEN
	assert.Equal(t, fx, float64(0))
}
//
func Test_mutateBounds_bothUpAndLowerAreExercised(t *testing.T){
	// GIVEN
	bounds := Bounds{midPointX : math.NaN(), lower: lowerBound, upper: upperBound}

	// WHEN
	newBounds0 := mutateBounds(bounds)
	newBounds1 := mutateBounds(newBounds0)

	//THEN
	assert.Equal(t, newBounds0.upper, float64(0))
	assert.Equal(t, newBounds1.lower, float64(-1))
}

func Test_findRootByIteration(t *testing.T) {
	// WHEN
	rootX := FindRootByIteration(lowerBound, upperBound)

	// THEN
	assert.Equal(t, rootX, float64(-0.35546875))
}

func Test_findRootByPrecision(t *testing.T) {
	// WHEN
	rootX := FindRootByPrecision(lowerBound, upperBound)

	// THEN
	assert.Equal(t, rootX, float64(-0.34375))
}
