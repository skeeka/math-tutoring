package roots

import (
	"github.com/magiconair/properties/assert"
	"testing"
)

/**
Used for testing: https://www.easycalculation.com/analytical/learn-least-square-regression.php
 */
func setup()[]Point{
	pt0 := Point{x: 60, y: 3.1}
	pt1 := Point{x: 61, y: 3.6}
	pt2 := Point{x: 62, y: 3.8}
	pt3 := Point{x: 63, y: 4}
	pt4 := Point{x: 65, y: 4.1}

	return []Point{pt0, pt1, pt2, pt3, pt4}
}

func Test_willSumXValues(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	xSum := SumXValues(points)

	// THEN
	assert.Equal(t, xSum, float32(311))
}

func Test_willSumYValues(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	xSum := SumYValues(points)

	// THEN
	assert.Equal(t, xSum, float32(18.6))
}

func Test_willSumXYValues(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	xSum := SumXYValues(points)

	// THEN
	assert.Equal(t, xSum, float32(1159.7))
}

func Test_willSumXSquaredValues(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	xSum := SumXSquaredValues(points)

	// THEN
	assert.Equal(t, xSum, float32(19359))
}

func Test_willFindLSRSlope(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	slope := CalculateLSRSlope(points)

	// THEN
	assert.Equal(t, slope, float32(0.18783651))
}

func Test_willFindLSRYIntercept(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	yIntercept := CalculateLSRYIntercept(points)

	// THEN
	assert.Equal(t, yIntercept, float32(-7.963431))
}

func Test_willCalculateApproxYForX(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	approxY := CalculateLSRYForX(points, 64)

	// THEN
	assert.Equal(t, approxY, float32(4.058106))
}

func Test_willCalculateApproxXForY(t *testing.T) {
	// GIVEN
	points := setup()

	// WHEN
	approxY := CalculateLSRXForY(points, 4)

	// THEN
	assert.Equal(t, approxY, float32(63.690655))
}

