package roots

type Point struct {
	x float32
	y float32
}


func SumXValues(points []Point) float32{
	sum := float32(0)
	for _, point := range points {
		sum += point.x
	}
	return sum
}

func SumYValues(points []Point) float32{
	sum := float32(0)
	for _, point := range points {
		sum += point.y
	}
	return sum
}

func SumXYValues(points []Point) float32{
	sum := float32(0)
	for _, point := range points {
		sum += point.y * point.x
	}
	return sum
}

func SumXSquaredValues(points []Point) float32{
	sum := float32(0)
	for _, point := range points {
		sum += point.x * point.x
	}
	return sum
}

/**
Slope = (N∑XY - (∑X)(∑Y)) / (N∑X2 - (∑X)2)
 */
func CalculateLSRSlope (p []Point) float32 {
	N := float32(len(p))
	return (N * SumXYValues(p) - SumXValues(p) * SumYValues(p)) / (N * SumXSquaredValues(p) - SumXValues(p) * SumXValues(p))
}

/**
Y Intercept = (∑Y - b(∑X)) / N
*/
func CalculateLSRYIntercept (p []Point) float32 {
	N := float32(len(p))
	return (SumYValues(p) - CalculateLSRSlope(p) * SumXValues(p)) / N
}

/**
Least Regression Equation(y) = a + bx
 */
func CalculateLSRYForX (p []Point, x float32) float32 {
	return CalculateLSRYIntercept(p) + CalculateLSRSlope(p) * x
}

/**
Least Regression Equation(x) = (y - a) / b
*/
func CalculateLSRXForY (p []Point, y float32) float32 {
	return  (y - CalculateLSRYIntercept(p)) / CalculateLSRSlope(p)
}
