package roots

import (
	"github.com/magiconair/properties/assert"
	"testing"
)

func Test_willRoundUpWhenBaseIs0(t *testing.T) { // this case should result in a 0 exponent
	// GIVEN
	log := Log{base: 10, result:0}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 1)
}

func Test_willRoundUpWhenBaseIsLargerThanResult(t *testing.T) { // this case should result in a fractional exponent
	// GIVEN
	log := Log{base: 10, result:5}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 1)
}

func Test_canFindExponentForBase10To10(t *testing.T) {
	// GIVEN
	log := Log{base: 10, result:10}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 1)
}

func Test_canFindExponentForBase10To100(t *testing.T) {
	// GIVEN
	log := Log{base: 10, result:100}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 2)
}

func Test_canFindExponentForBase10To110(t *testing.T) {
	// GIVEN
	log := Log{base: 10, result:110}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 2)
}

func Test_canFindExponentForBase6To216(t *testing.T) {
	// GIVEN
	log := Log{base: 6, result:216}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 3)
}

func Test_canFindExponentForBase6To288(t *testing.T) {
	// GIVEN
	log := Log{base: 6, result:256}

	// WHEN
	exponent := CalculateExponent(log)

	// THEN
	assert.Equal(t, exponent, 3)
}
