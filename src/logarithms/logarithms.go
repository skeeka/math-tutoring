package roots

type Log struct {
	base int
	result int
}

func CalculateExponent (log Log) int {
	var tryResult = log.base
	var exponent = 1
	for tryResult < log.result {
		tryResult = tryResult * log.base
		if tryResult <= log.result {
			exponent++
		}
	}
	return exponent
}