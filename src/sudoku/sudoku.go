package roots

import (
	"fmt"
	"math/rand"
	"time"
)

type Log struct {
	base int
	result int
}

func getRandomNumber0To9 () int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Int()%10
}

func GetRandomArray0To9 () [][]int {
	a := make([][]int, 9)
	for i := range a {
		a[i] = make([]int, 9)
	}
	for _, h := range a {
		for i := range h {
			h[i] = getRandomNumber0To9 ()
		}
	}
	return a
}

func isUniqueRow (row []int) bool {
	keys := make(map[int]bool)
	var list []int
	for _, entry := range row {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}else{
			return false
		}
	}
	return true
}

func WhichSudokuRowsAreUnique(sudoku [][]int) []int {
	var uniqueRows []int
	for i, row := range sudoku {
		if isUniqueRow(row) {
			uniqueRows = append(uniqueRows, i)
		}
	}
	return uniqueRows
}

func PrettyPrintSudoku(a [][]int){
	for _, h := range a {
		for _, cell := range h {
			fmt.Print(cell, " ")
		}
		fmt.Println()
	}
}