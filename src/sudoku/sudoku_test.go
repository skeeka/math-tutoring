package roots

import (
	"fmt"
	"github.com/magiconair/properties/assert"
	"testing"
)

func Test_willCreate9x9SudokuAndTestForUniqueRows(t *testing.T) { // this case should result in a 0 exponent
	for i := 0; i < 100000; i++ {
		// GIVEN
		sudoku := GetRandomArray0To9()

		// WHEN
		uniqueRows := WhichSudokuRowsAreUnique(sudoku)
		if len(uniqueRows) > 2 {
			fmt.Println("")
			PrettyPrintSudoku(sudoku)
			fmt.Print("-unique-rows-----")
			fmt.Print(uniqueRows)
			fmt.Print("--")
		}
	}
}

func Test_willFindUniqueRowsInSudoku(t *testing.T) {
	// GIVEN
	sudoku := [][]int{
		{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, // unique
		{4, 5, 6, 7, 7, 3, 7, 8, 5, 4},
		{0, 1, 2, 3, 4, 8, 6, 7, 5, 9}, // unique
		{1, 2, 8, 7, 7, 1, 7, 2, 0, 4},
		{1, 2, 6, 7, 7, 4, 0, 4, 7, 4},
		{7, 3, 1, 1, 6, 7, 7, 3, 0, 4},
		{1, 2, 6, 7, 7, 5, 7, 5, 9, 4},
		{1, 0, 2, 3, 4, 8, 6, 7, 5, 9}, // unique
		{1, 9, 2, 7, 2, 2, 7, 9, 4, 4},
		{1, 9, 2, 3, 2, 2, 7, 9, 5, 4},
	}

	// WHEN
	uniqueRows := WhichSudokuRowsAreUnique(sudoku)

	// THEN
	fmt.Print("-unique-rows-----")
	fmt.Print(uniqueRows)
	fmt.Print("--")
	assert.Equal(t, uniqueRows[0], 0)
	assert.Equal(t, uniqueRows[1], 2)
	assert.Equal(t, uniqueRows[2], 7)
}

func Test_willReturnTrueForUniqueRowInArray(t *testing.T) {
	// GIVEN
	uniqueRow := []int{4, 1, 7, 3, 0, 6, 9, 5, 8, 2}

	// WHEN
	unique := isUniqueRow(uniqueRow)

	// THEN
	assert.Equal(t, unique, true)
}

func Test_willReturnFalseForNonUniqueRowInArray(t *testing.T) {
	// GIVEN
	uniqueRow := []int{4, 1, 7, 5, 0, 6, 9, 5, 8, 2}

	// WHEN
	unique := isUniqueRow(uniqueRow)

	// THEN
	assert.Equal(t, unique, false)
}
